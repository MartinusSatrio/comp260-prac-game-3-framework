﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {
	public float speed = 10.0f;
	public Vector3 direction;
	public float lifeTime = 2.0f;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = speed * direction;
		lifeTime -= Time.deltaTime;
		if(lifeTime == 0.0f){
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}
}
