﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {
	public GameObject ShellPanel;
	private bool paused = true;
	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullscreenToggle;
	public Slider volumeSlider;
	// Use this for initialization
	void Start () {
		optionsPanel.SetActive (false);
		SetPaused (paused);	

		//populate the list of video quality levels
		qualityDropdown.ClearOptions();
		List<string> names = new List<string> ();
		for(int i = 0; i < QualitySettings.names.Length; i++){
			names.Add (QualitySettings.names[i]);
		}
		qualityDropdown.AddOptions (names);
	}
	
	// Update is called once per frame
	void Update () {
		if(!paused && Input.GetKeyDown(KeyCode.Escape)){
			SetPaused (true);
		}
	}

	private void SetPaused(bool p){
		paused = p;
		ShellPanel.SetActive (paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay(){
		SetPaused (false);
	}

	public void OnPressedQuit(){
		Application.Quit ();
	}

	public void OnPressedOptions(){
		ShellPanel.SetActive (false);
		optionsPanel.SetActive (true);
	}

	public void OnPressedCancel(){
		ShellPanel.SetActive (true);
		optionsPanel.SetActive (false);
	}

	public void OnPressedApply(){
		//apply changes
		QualitySettings.SetQualityLevel(qualityDropdown.value);

		ShellPanel.SetActive (true);
		optionsPanel.SetActive (false);
	}
}
